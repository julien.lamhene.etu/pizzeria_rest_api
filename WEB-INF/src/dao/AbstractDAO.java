package dao;

import io.jsonwebtoken.Claims;

import java.sql.*;
import java.util.Base64;

public abstract class AbstractDAO {
	// Vérifie le jeton de connexion avec la base de données par la méthode d'Authentification Basic
	public boolean verifToken(String token) {
		if(token == null || token.isEmpty()) {
			return false;
		}
		byte[] decoder = Base64.getDecoder().decode(token);
		String[] splits = new String(decoder).split(":");
		String login = splits[0];
		String mdp = splits[1];
		DS ds = new DS();
		Connection con = null;
		try {
			con = ds.getConnection();
			String query = "SELECT * FROM users WHERE LOGIN=? AND MDP=?";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, login);
			ps.setString(2, mdp);
			System.out.println("Basic : " + ps);
			ResultSet rs = ps.executeQuery();
			return rs.next();
		}catch(Exception e) {
			try {
				con.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return false;
	}

	// Vérifie le jeton de connexion avec la base de données par la méthode d'Authentification Bearer
	public boolean verifTokenJWT(String token) {
		DS ds = new DS();
		Connection con = null;
		try {
			con = ds.getConnection();
			Claims claim = JwtManager.decodeJWT(token);
			String[] splits = claim.getIssuer().split(":");
			String login = claim.getSubject();
			String mdp = splits[1];
			String query = "SELECT * FROM users WHERE LOGIN=? AND MDP=?";
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, login);
			ps.setString(2, mdp);
			ResultSet rs = ps.executeQuery();
			return rs.next();
		}catch(Exception e) {
			try {
				con.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return false;
	}
}