package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import controleurs.*;

public class CommandesDAO extends AbstractDAO{
    private final DS ds;
    
    public CommandesDAO(DS ds){
        this.ds=ds;
    }

    public Commandes find(int id){
        Connection con = null;
        Commandes commande = null;
        PizzasDAO pizzaDAO = new PizzasDAO(ds);
        try{
            con = ds.getConnection();
            String query = "SELECT * FROM COMMANDES C JOIN LIVRAISONS L USING(id_cmd) JOIN PIZZAS P ON L.PZO=P.PZO WHERE ID_CMD = ?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
            	commande = new Commandes(id, rs.getInt("cno"), rs.getString("date_cmd"));
            	rs = ps.executeQuery();
            	while(rs.next()) {
            		Pizzas pizzaAdded = pizzaDAO.find(rs.getInt("pzo"));//new Pizzas(rs.getInt("pzo"), rs.getString("name"), rs.getString("pate"), rs.getDouble("price"));
            		commande.getPizzas().add(pizzaAdded);            		
            	}
            }
            con.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return commande;
    }
    
    public double getFinalPrice(int idCmd) {
    	Connection con = null;
        double finalPrice = 0;
        PizzasDAO pizzaDao = new PizzasDAO(ds);
        try{
            con = ds.getConnection();
            String query = "SELECT * FROM COMMANDES C JOIN LIVRAISONS L USING(id_cmd) JOIN PIZZAS P ON L.PZO=P.PZO WHERE ID_CMD = ?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1,idCmd);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
            	finalPrice += pizzaDao.getFinalPrice(rs.getInt("pzo"));
            }
            con.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    	return finalPrice;
    }
    
    public List<Commandes> findAll(){
        Connection con = null;
        List<Commandes> commandes = new ArrayList<>();
        Commandes commande = null;
        try{
            con = ds.getConnection();
            String query = "SELECT * FROM COMMANDES C JOIN LIVRAISONS L USING(id_cmd) JOIN PIZZAS P ON L.PZO=P.PZO";
            PreparedStatement ps = con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
            	commande = new Commandes(rs.getInt("id_cmd"), rs.getInt("cno"), rs.getString("date_cmd"));
            	commande.getPizzas().add(new Pizzas(rs.getInt("pzo"), rs.getString("name"), rs.getString("pate"), rs.getDouble("price")));
            	commandes.add(commande);
            }
            con.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return commandes;
    }
    
    public void save(Commandes commande){
        Connection con = null;
        try{
            con = ds.getConnection();
            String query = "INSERT INTO COMMANDES VALUES(?,?,?)";
            String query2 = "INSERT INTO LIVRAISONS VALUES(?,?)";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1,commande.getId_cmd());
            ps.setInt(2,commande.getCno());
            ps.setString(3,commande.getDate());
            ps.executeUpdate();
            for (Pizzas pizzas : commande.getPizzas()) {
            	ps = con.prepareStatement(query2);
            	ps.setInt(1,commande.getId_cmd());
                ps.setInt(2,pizzas.getPzo());
                ps.executeUpdate();
			}
            con.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public void delete(int id) {
    	Connection con = null;
        try{
            con = ds.getConnection();
            String query = "DELETE FROM COMMANDES WHERE ID_CMD = ?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1,id);
            ps.executeUpdate();
            con.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}