package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import controleurs.*;

public class IngredientDAO extends AbstractDAO{
    private final DS ds;
    
    public IngredientDAO(DS ds){
        this.ds=ds;
    }

    public Ingredient find(int id){
        Connection con = null;
        Ingredient ingredient = null;
        try{
            con = ds.getConnection();
            String query = "SELECT * FROM INGREDIENTS WHERE ID = ?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
            	ingredient = new Ingredient(id, rs.getString("name"), rs.getDouble("price"));
            }
            con.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return ingredient;
    }
    
    public List<Ingredient> findAll(){
        Connection con = null;
        List<Ingredient> ingredients = new ArrayList<>();
        Ingredient ingredient = null;
        try{
            con = ds.getConnection();
            String query = "SELECT * FROM INGREDIENTS";
            PreparedStatement ps = con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
            	ingredient = new Ingredient(rs.getInt("id"), rs.getString("name"), rs.getDouble("price"));
            	ingredients.add(ingredient);
            }
            con.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return ingredients;
    }
    
    public void save(Ingredient ingredient){
        Connection con = null;
        try{
            con = ds.getConnection();
            String query = "INSERT INTO INGREDIENTS VALUES(?,?,?)";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1,ingredient.getId());
            ps.setString(2,ingredient.getName());
            ps.setDouble(3,ingredient.getPrice());
            ps.executeUpdate();
            con.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public void delete(int id) {
    	Connection con = null;
        try{
            con = ds.getConnection();
            String query = "DELETE FROM INGREDIENTS WHERE ID = ?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1,id);
            ps.executeUpdate();
            con.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}