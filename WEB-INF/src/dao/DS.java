package dao;

import java.sql.*;

public class DS {
    public static final DS instance  = new DS();
    
    DS() { 
        // gestion des exceptions
       try{
            Class.forName("org.postgresql.Driver");
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    public Connection getConnection(){
        Connection con = null;
        try{
        	/*
        	 * À utiliser à l'IUT
	            String url = "jdbc:postgresql://psqlserv:5432/but2";
	            String nom = "julienlamheneetu";
	            String mdp = "moi";
        	 */
            /* 
             * À utiliser sur ordinateur personnel
            */
        	String url = "jdbc:postgresql://localhost:5432/julien";
        	String nom = "julien";
        	String mdp = "julien";
            con = DriverManager.getConnection(url,nom,mdp);
        }catch(Exception e){
        	e.printStackTrace();
        }

        return con;
    }
}