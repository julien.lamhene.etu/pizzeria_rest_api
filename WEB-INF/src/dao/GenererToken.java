package dao;

import io.jsonwebtoken.Claims;

import java.sql.*;
import java.util.Base64;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.WebServlet;

@WebServlet("/users/token")
public class GenererToken extends HttpServlet {
	@Serial
    private static final long serialVersionUID = 1L;

	public void service( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
        res.setContentType("text/html;charset=UTF-8");
		//res.setContentType("application/json;charset=UTF-8");
        PrintWriter out = res.getWriter();
        String login = req.getParameter("login");
        String password = req.getParameter("mdp");
        String authentificationBy = "basic";
        if(req.getParameter("authentification") != null && !req.getParameter("authentification").isEmpty()) {
            authentificationBy = req.getParameter("authentification");
        }
        DS ds = new DS();
        try{
        	Connection con = ds.getConnection();
            String query = "SELECT * FROM users WHERE LOGIN=? AND MDP=?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, login);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            out.println(ps);
            if(rs.next()) {
                String token = login+":"+password;
                String encoder = "";
                if(authentificationBy.equalsIgnoreCase("basic")) {
                    encoder = Base64.getEncoder().encodeToString(token.getBytes());
                    out.println("La méthode utilisé est : Basic");
                }else {
                    encoder = JwtManager.createJWT(login, token);
                    out.println("La méthode utilisé est : Bearer");
                    Claims claims = JwtManager.decodeJWT(encoder);
                    out.println("Votre subject est : "+claims.getSubject());
                    out.println("Votre issuer est : "+claims.getIssuer());
                }
                out.println("Votre token est : "+encoder);
            }
            con.close();
        }catch(Exception e){
        	/*out.println("<h2> Vous êtes inconnue ici </h2>");*/
            //out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}