package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import controleurs.*;

public class PizzasDAO extends AbstractDAO{
    private final DS ds;
    
    public PizzasDAO(DS ds){
        this.ds=ds;
    }

    public Pizzas find(int id){
        Connection con = null;
        Pizzas pizzas = null;
        try{
            con = ds.getConnection();
            String query = "SELECT P.PZO, P.NAME as pizza_name, P.PATE, P.PRICE, I.ID, I.NAME as ingredients, I.PRICE as ingredientPrice FROM PIZZAS P "
            		+ "JOIN GARNITURE G ON P.PZO=G.PZO "
            		+ "JOIN INGREDIENTS I ON G.IGO=I.ID "
            		+ "WHERE P.PZO= ?";
            //Une table garniture existe pour pouvoir lister les ingrédients d'une pizza
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
            	pizzas = new Pizzas(id, rs.getString("pizza_name"), rs.getString("pate"), rs.getInt("price"));
            	rs = ps.executeQuery();
            	while(rs.next()) {
            		pizzas.getGarniture().add(new Ingredient(rs.getInt("id"), rs.getString("ingredients"), rs.getDouble("ingredientPrice")));            		
            	}
            }
            con.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return pizzas;
    }
    
    public Ingredient findIngredient(int idPizza, int idIng) {
    	Connection con = null;
        Ingredient ing = null;
        try{
            con = ds.getConnection();
            String query = "SELECT I.ID, I.NAME as ingredients, I.PRICE as ingredientPrice FROM PIZZAS P "
            		+ "JOIN GARNITURE G ON P.PZO=G.PZO "
            		+ "JOIN INGREDIENTS I ON G.IGO=I.ID "
            		+ "WHERE P.PZO= ? AND I.ID = ?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1,idPizza);
            ps.setInt(2,idIng);
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
            	ing = new Ingredient(idIng, rs.getString("ingredients"), rs.getDouble("ingredientPrice"));
            }
            con.close();
        }catch(Exception e){
            //System.out.println(e.getMessage());
        	e.printStackTrace();
        }
        return ing;
    }
    
    public List<Pizzas> findAll(){
        Connection con = null;
        List<Pizzas> pizzas = new ArrayList<>();
        Pizzas pizza = null;
        try{
            con = ds.getConnection();
            String query = "SELECT P.PZO, P.NAME as pizza_name, P.PATE, P.PRICE, I.ID, I.NAME as ingredients, I.PRICE as ingredientPrice FROM PIZZAS P JOIN GARNITURE G ON P.PZO=G.PZO JOIN INGREDIENTS I ON G.IGO=I.ID";
            PreparedStatement ps = con.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
            	pizza = new Pizzas(rs.getInt("pzo"), rs.getString("pizza_name"), rs.getString("pate"), rs.getDouble("price"));
            	pizza.getGarniture().add(new Ingredient(rs.getInt("id"), rs.getString("ingredients"), rs.getDouble("ingredientPrice")));
            	pizzas.add(pizza);
            }
            con.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return pizzas;
    }
    
    public double getFinalPrice(int id) {
    	Connection con = null;
        double finalPrice = 0;
        try{
            con = ds.getConnection();
            String query = "SELECT (SUM(I.PRICE)+P.PRICE) final_price FROM PIZZAS P "
            		+ "JOIN GARNITURE G USING(PZO) "
            		+ "JOIN INGREDIENTS I ON G.IGO=I.ID "
            		+ "WHERE P.PZO=? "
            		+ "GROUP BY PZO";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
            	finalPrice = rs.getDouble("final_price");            	
            }
            con.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    	return finalPrice;
    }
    
    public void save(Pizzas pizzas){
        Connection con = null;
        try{
            con = ds.getConnection();
            String query = "INSERT INTO PIZZAS VALUES(?,?,?)";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1,pizzas.getPzo());
            ps.setString(2,pizzas.getName());
            ps.setString(3,pizzas.getPate());
            //ps.executeUpdate();
            if(ps.executeUpdate() > 0){
                for (Ingredient ingredient : pizzas.getGarniture()) {
                    String query2 = "INSERT INTO GARNITURE VALUES(?,?)";
                    ps = con.prepareStatement(query2);
                    ps.setInt(1,pizzas.getPzo());
                    ps.setInt(2,ingredient.getId());
                    ps.executeUpdate();
                }
            }
            con.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public void addIngredient(Pizzas pizza, Ingredient ingredient) {
    	Connection con = null;
        try{
            con = ds.getConnection();
            String query = "INSERT INTO GARNITURE VALUES(?,?)";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1, pizza.getPzo());
            ps.setInt(2, ingredient.getId());
            ps.executeUpdate();
            con.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public void delete(int id) {
    	Connection con = null;
        try{
            con = ds.getConnection();
            String query = "DELETE FROM PIZZAS WHERE PZO = ?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setInt(1,id);
            System.out.println(ps);
            ps.executeUpdate();
            con.close();
        }catch(Exception e){
            //System.out.println(e.getMessage());
        	e.printStackTrace();
        }
    }
    
    public void removeIngredient(int idPizza, int idIngredient) {
    	Connection con = null;
    	try{
    		con = ds.getConnection();
    		String query = "DELETE FROM GARNITURE WHERE PZO = ? AND IGO = ? ";
    		PreparedStatement ps = con.prepareStatement(query);
    		ps.setInt(1, idPizza);
    		ps.setInt(2, idIngredient);
    		ps.executeUpdate();
    		con.close();
    	}catch(Exception e){
    		System.out.println(e.getMessage());
    	}
    }
    
    public void update(Pizzas pizza) {
    	Connection con = null;
        try{
            con = ds.getConnection();
            String query = "UPDATE PIZZAS SET name = ?, pate = ?, price = ? WHERE pzo = ?";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, pizza.getName());
            ps.setString(2, pizza.getPate());
            ps.setDouble(3, pizza.getPrice());
            ps.setInt(4, pizza.getPzo());
            System.out.println(ps);
            ps.executeUpdate();
            con.close();
        }catch(Exception e){
            //System.out.println(e.getMessage());
        	e.printStackTrace();
        }
    }
}