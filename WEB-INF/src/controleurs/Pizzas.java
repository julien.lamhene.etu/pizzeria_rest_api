package controleurs;

import java.util.ArrayList;
import java.util.List;

public class Pizzas {
	protected int pzo; //Identifiant des pizzas;
	protected String name;
	protected String pate;
	protected List<Ingredient> garniture = new ArrayList<>();
	protected double initPrice;
	protected double finalPrice;
	
	public Pizzas() {
	}

	public Pizzas(int pzo, String name, String pate, double price) {
		this.pzo = pzo;
		this.name = name;
		this.pate = pate;
		this.initPrice = price;
	}

	public Pizzas(int pzo, String name, String pate, double price, List<Ingredient> garniture) {
		this(pzo,name,pate,price);
		this.garniture = garniture;
	}
	
	public int getPzo() {
		return pzo;
	}
	public void setPzo(int pzo) {
		this.pzo = pzo;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getPate() {
		return pate;
	}
	public void setPate(String pate) {
		this.pate = pate;
	}

	public List<Ingredient> getGarniture() {
		return garniture;
	}
	public void setGarniture(List<Ingredient> garniture) {
		this.garniture = garniture;
	}

	
	public double getPrice() {
		return initPrice;
	}

	public void setPrice(double price) {
		this.initPrice = price;
	}
	
	
}
