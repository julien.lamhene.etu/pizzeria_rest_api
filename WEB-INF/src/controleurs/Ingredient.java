package controleurs;

public class Ingredient {
	protected int id;
	protected String name;
	protected double price;
	
	public Ingredient() {

	}

	public Ingredient(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public Ingredient(int id, String name, double price) {
		this(id,name);
		this.price = price;
	}	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}
