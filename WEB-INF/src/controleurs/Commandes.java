package controleurs;

import java.util.ArrayList;
import java.util.List;

public class Commandes {
	protected int id_cmd; // identidiants commandes
	protected int cno; // identifiant clients
	protected String date;
	protected List<Pizzas> pizzas = new ArrayList<>();
	
	public Commandes() {
		
	}

	public Commandes(int id_cmd, int cno, String date) {
		this.id_cmd = id_cmd;
		this.cno = cno;
		this.date = date;
	}
	

	public Commandes(int id_cmd, int cno, String date, List<Pizzas> pizzas) {
		this(id_cmd,cno,date);
		this.pizzas = pizzas;
	}

	public int getCno() {
		return cno;
	}

	public void setCno(int cno) {
		this.cno = cno;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getId_cmd() {
		return id_cmd;
	}

	public void setId_cmd(int id_cmd) {
		this.id_cmd = id_cmd;
	}

	public List<Pizzas> getPizzas() {
		return pizzas;
	}

	public void setPizzas(List<Pizzas> pizzas) {
		this.pizzas = pizzas;
	}

}
