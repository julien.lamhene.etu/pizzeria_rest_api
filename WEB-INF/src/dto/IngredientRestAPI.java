package dto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serial;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import controleurs.Ingredient;
import dao.*;

@WebServlet("/ingredients/*")
public class IngredientRestAPI extends HttpServlet {
	@Serial
	private static final long serialVersionUID = 1L;
	
	private IngredientDAO ingredientsDAO;
			
	public void init(ServletConfig config) {
		try{
			DS ds = DS.instance;
			ingredientsDAO = new IngredientDAO(ds);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		res.setContentType("application/json;charset-UTF-8");
		ObjectMapper mapper = new ObjectMapper();
		PrintWriter out = res.getWriter();
		List<Ingredient> ingredients = ingredientsDAO.findAll();
		String info = req.getPathInfo();
		String authorization = req.getHeader("Authorization");
		String token = "";
		boolean isAuthorized;
		if (authorization == null || (!authorization.startsWith("Basic") && !authorization.startsWith("Bearer"))) {
			res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}else {
			if(authorization.startsWith("Bearer")) {
				token = authorization.substring("Bearer".length()).trim();
				isAuthorized = ingredientsDAO.verifTokenJWT(token);
			}else {
				token = authorization.substring("Basic".length()).trim();
				isAuthorized = ingredientsDAO.verifToken(token);
			}
			if(isAuthorized) {
				String[] splits;
				try {
					splits = info.split("/");
				}catch(Exception e) {
					splits = new String[1];
				}
				if (info == null || info.equals("/")) {
					out.println(mapper.writeValueAsString(ingredients));
				} else if(splits.length >= 2) {
					int id;
					String name = "";
					try {
						id = Integer.parseInt(splits[1]);
						if(splits.length == 3) {
							name = splits[2];					
						}
					}catch(Exception e) {
						res.sendError(HttpServletResponse.SC_BAD_REQUEST);
						return;
					}
					
					if(ingredientsDAO.find(id) != null) {
						if(!name.equals("")) {
							out.println(mapper.writeValueAsString(ingredientsDAO.find(id).getName()));
						}else {
							out.println(mapper.writeValueAsString(ingredientsDAO.find(id)));
						}
					}else {
						res.sendError(HttpServletResponse.SC_NOT_FOUND);				
					}
				}else {
					res.sendError(HttpServletResponse.SC_BAD_REQUEST);
				}
			} else {
				res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			}
		}
	}
	
	public void doPost( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		res.setContentType("application/json;charset-UTF-8");
		PrintWriter out = res.getWriter();
		String jsonToString;
		ObjectMapper mapper = new ObjectMapper();	
		StringBuilder data = new StringBuilder();
		BufferedReader reader = req.getReader();
		String line;
		Ingredient nouvIngred = null;
		String token = "";
		String authorization = req.getHeader("Authorization");
		boolean isAuthorized;
		if (authorization == null || (!authorization.startsWith("Basic") && !authorization.startsWith("Bearer"))) {
			res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}else {
			if(authorization.startsWith("Bearer")) {
				token = authorization.substring("Bearer".length()).trim();
				isAuthorized = ingredientsDAO.verifTokenJWT(token);
			}else {
				token = authorization.substring("Basic".length()).trim();
				isAuthorized = ingredientsDAO.verifToken(token);
			}if(isAuthorized) {
				while ((line = reader.readLine()) != null) {
					data.append(line);
				}
				try {
					nouvIngred = mapper.readValue(data.toString(), Ingredient.class);				
				}catch(Exception e) {
					res.sendError(HttpServletResponse.SC_BAD_REQUEST);
					return;
				}
				if(ingredientsDAO.find(nouvIngred.getId())!= null) {
					res.sendError(HttpServletResponse.SC_CONFLICT);
                    return;
				}
				ingredientsDAO.save(nouvIngred);
			} else {
				res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			}	
		}
		jsonToString = mapper.writeValueAsString(nouvIngred);
		out.println(jsonToString);
		out.close();
	}
	
	public void doDelete( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		res.setContentType("application/json;charset-UTF-8");
		PrintWriter out = res.getWriter();
		ObjectMapper mapper = new ObjectMapper();
		String info = req.getPathInfo();
		String[] splits;
		String authorization = req.getHeader("Authorization");
		String token = "";
		boolean isAuthorized;
		if (authorization == null || (!authorization.startsWith("Basic") && !authorization.startsWith("Bearer"))) {
			res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}else {
			if(authorization.startsWith("Bearer")) {
				token = authorization.substring("Bearer".length()).trim();
				isAuthorized = ingredientsDAO.verifTokenJWT(token);
			}else {
				token = authorization.substring("Basic".length()).trim();
				isAuthorized = ingredientsDAO.verifToken(token);
			}if(isAuthorized) {
				try {
					splits = info.split("/");
				}catch(Exception e) {
					splits = new String[1];
				}
		
				if(splits.length == 2) {
					int id;
					try {
						id = Integer.parseInt(splits[1]);
					}catch(Exception e) {
						res.sendError(HttpServletResponse.SC_BAD_REQUEST);
						return;
					}
					
					if(ingredientsDAO.find(id) != null) {
						ingredientsDAO.delete(id);
						out.println(mapper.writeValueAsString(ingredientsDAO.find(id)));	
					}else {
						res.sendError(HttpServletResponse.SC_NOT_FOUND);				
					}
					
				}
			} else {
				res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
				out.println("Erreur d'authentification");
			}	
		}
	}
}
