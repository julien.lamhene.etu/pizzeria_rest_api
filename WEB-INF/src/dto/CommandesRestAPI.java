package dto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import controleurs.Commandes;
import dao.*;

@WebServlet("/commandes/*")
public class CommandesRestAPI extends HttpServlet {
	private CommandesDAO commandesDAO;
			
	public void init(ServletConfig config) {
		try{
			DS ds = DS.instance;
			commandesDAO = new CommandesDAO(ds);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		res.setContentType("application/json;charset-UTF-8");
		ObjectMapper mapper = new ObjectMapper();
		PrintWriter out = res.getWriter();
		List<Commandes> commandes = commandesDAO.findAll();
		String info = req.getPathInfo();
		String[] splits;
		String authorization = req.getHeader("Authorization");
		String token = "";
		boolean isAuthorized;
		if (authorization == null || (!authorization.startsWith("Basic") && !authorization.startsWith("Bearer"))) {
			res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}else {
			if(authorization.startsWith("Bearer")) {
				token = authorization.substring("Bearer".length()).trim();
				isAuthorized = commandesDAO.verifTokenJWT(token);
			}else {
				token = authorization.substring("Basic".length()).trim();
				isAuthorized = commandesDAO.verifToken(token);
			}
			if(isAuthorized) {
				try {
					splits = info.split("/");
				}catch(Exception e) {
					splits = new String[1];
				}
				
				if (info == null || info.equals("/")) {
					out.println(mapper.writeValueAsString(commandes));
				}
		
				if(splits.length >= 2) {
					int id;
					String finalPrice = "";
					try {
						id = Integer.parseInt(splits[1]);
						if(splits.length == 3) {
							finalPrice = splits[2];
						}
					}catch(Exception e) {
						res.sendError(HttpServletResponse.SC_BAD_REQUEST);
						return;
					}
					if(commandesDAO.find(id) != null) {
						if(finalPrice.equalsIgnoreCase("prixfinal")) {
							out.println(mapper.writeValueAsString("Le prix final de la commande " + commandesDAO.find(id).getId_cmd() + " est " + commandesDAO.getFinalPrice(id)));
						}else {
							out.println(mapper.writeValueAsString(commandesDAO.find(id)));
						}
					}else {
						res.sendError(HttpServletResponse.SC_NOT_FOUND);				
					}
				}
			} else {
				res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			}
		}
	}
	
	public void doPost( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		res.setContentType("application/json;charset-UTF-8");
		PrintWriter out = res.getWriter();
		String jsonToString;
		ObjectMapper mapper = new ObjectMapper();	
		StringBuilder data = new StringBuilder();
		BufferedReader reader = req.getReader();
		String line;
		Commandes nouvCommd = null;
		String authorization = req.getHeader("Authorization");
		String token = "";
		boolean isAuthorized;
		if (authorization == null || (!authorization.startsWith("Basic") && !authorization.startsWith("Bearer"))) {
			res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}else {
			if(authorization.startsWith("Bearer")) {
				token = authorization.substring("Bearer".length()).trim();
				isAuthorized = commandesDAO.verifTokenJWT(token);
			}else {
				token = authorization.substring("Basic".length()).trim();
				isAuthorized = commandesDAO.verifToken(token);
			}
			if(isAuthorized) {
				while ((line = reader.readLine()) != null) {
					data.append(line);
				}
				try {
					nouvCommd = mapper.readValue(data.toString(), Commandes.class);

				}catch(Exception e) {
					res.sendError(HttpServletResponse.SC_BAD_REQUEST);
					return;
				}
			} else {
				res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			}
			if(commandesDAO.find(nouvCommd.getId_cmd()) != null) {
				res.sendError(HttpServletResponse.SC_CONFLICT);
				return;
			}
			commandesDAO.save(nouvCommd);
		}
		jsonToString = mapper.writeValueAsString(nouvCommd);
		out.println(jsonToString);
		out.println(mapper.writeValueAsString(data.toString()));
		out.close();
	}
	
	public void doDelete( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		res.setContentType("application/json;charset-UTF-8");
		PrintWriter out = res.getWriter();
		ObjectMapper mapper = new ObjectMapper();
		String info = req.getPathInfo();
		String[] splits;
		String authorization = req.getHeader("Authorization");
		String token = "";
		boolean isAuthorized;
		if (authorization == null || (!authorization.startsWith("Basic") && !authorization.startsWith("Bearer"))) {
			res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}else {
			if(authorization.startsWith("Bearer")) {
				token = authorization.substring("Bearer".length()).trim();
				isAuthorized = commandesDAO.verifTokenJWT(token);
			}else {
				token = authorization.substring("Basic".length()).trim();
				isAuthorized = commandesDAO.verifToken(token);
			}
			if(isAuthorized) {
				try {
					splits = info.split("/");
				}catch(Exception e) {
					splits = new String[1];
				}
		
				if(splits.length == 2) {
					int id;
					try {
						id = Integer.parseInt(splits[1]);
					}catch(Exception e) {
						res.sendError(HttpServletResponse.SC_BAD_REQUEST);
						return;
					}
					
					if(commandesDAO.find(id) != null) {
						commandesDAO.delete(id);
						out.println(mapper.writeValueAsString(commandesDAO.find(id)));	
					}else {
						res.sendError(HttpServletResponse.SC_NOT_FOUND);				
					}
				}
			} else {
				res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
				out.println("Erreur d'authentification");
			}	
		}
	}
}
