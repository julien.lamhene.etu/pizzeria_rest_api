package dto;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

public abstract class AbstractRestAPI extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		if (req.getMethod().equalsIgnoreCase("PATCH")) {
			doPatch(req, res);
		} else {
			super.service(req, res);
		}
	}

	public abstract void doPatch(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException;
}