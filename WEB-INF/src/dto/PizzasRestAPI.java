package dto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serial;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import controleurs.Ingredient;
import controleurs.Pizzas;
import dao.*;

@WebServlet("/pizzas/*")
public class PizzasRestAPI extends AbstractRestAPI {
	@Serial
	private static final long serialVersionUID = 1L;
	
	private PizzasDAO pizzaDAO;
	private IngredientDAO ingredientDAO;
			
	public void init(ServletConfig config) {
		try{
			DS ds = DS.instance;
			pizzaDAO = new PizzasDAO(ds);
			ingredientDAO = new IngredientDAO(ds);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		res.setContentType("application/json;charset-UTF-8");
		ObjectMapper mapper = new ObjectMapper();
		PrintWriter out = res.getWriter();
		List<Pizzas> pizzas = pizzaDAO.findAll();
		String info = req.getPathInfo();
		String[] splits;
		String authorization = req.getHeader("Authorization");
		String token = "";
		boolean isAuthorized;
		if (authorization == null || (!authorization.startsWith("Basic") && !authorization.startsWith("Bearer"))) {
			res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}else {
			if(authorization.startsWith("Bearer")) {
				token = authorization.substring("Bearer".length()).trim();
				isAuthorized = pizzaDAO.verifTokenJWT(token);
			}else {
				token = authorization.substring("Basic".length()).trim();
				isAuthorized = pizzaDAO.verifToken(token);
			}
			if(isAuthorized) {
				try {
					splits = info.split("/");
				}catch(Exception e) {
					splits = new String[1];
				}
				if (info == null || info.equals("/")) {
					out.println(mapper.writeValueAsString(pizzas));
				}else if(splits.length >= 2) {
					int id;
					String finalPrice = "";
					try {
						id = Integer.parseInt(splits[1]);
						if(splits.length == 3) {
							finalPrice = splits[2];
						}
					}catch(Exception e) {
						res.sendError(HttpServletResponse.SC_BAD_REQUEST);
						return;
					}
					if(pizzaDAO.find(id) != null) {
						if(finalPrice.equalsIgnoreCase("prixfinal")) {
							out.println(mapper.writeValueAsString("Le prix final de la Pizza " + pizzaDAO.find(id).getPzo() + " est " +pizzaDAO.getFinalPrice(id)));
						}else {
							out.println(mapper.writeValueAsString(pizzaDAO.find(id)));
						}
					}else {
						res.sendError(HttpServletResponse.SC_NOT_FOUND);				
					}
				}else {
					res.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
				}
			} else {
				res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
				out.println("Erreur d'authentification");
			}
			
		}
	}
	@Override
	public void doPost( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		res.setContentType("application/json;charset-UTF-8");
		PrintWriter out = res.getWriter();
		String jsonToString;
		ObjectMapper mapper = new ObjectMapper();	
		StringBuilder data = new StringBuilder();
		BufferedReader reader = req.getReader();
		String info = req.getPathInfo();
		String[] splits;
		Pizzas nouvPizza = null;
		String line;
		String authorization = req.getHeader("Authorization");
		String token = "";
		boolean isAuthorized;
		if (authorization == null || (!authorization.startsWith("Basic") && !authorization.startsWith("Bearer"))) {
			res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}else {
			if(authorization.startsWith("Bearer")) {
				token = authorization.substring("Bearer".length()).trim();
				isAuthorized = pizzaDAO.verifTokenJWT(token);
			}else {
				token = authorization.substring("Basic".length()).trim();
				isAuthorized = pizzaDAO.verifToken(token);
			}
			if(isAuthorized) {
				while ((line = reader.readLine()) != null) {
					data.append(line);
				}
				try {
					splits = info.split("/");
				}catch(Exception e) {
					splits = new String[1];
				}
				if(splits.length >= 2) {
					int pizzaId;
					int ingredientId = -1;
					try {
						pizzaId = Integer.parseInt(splits[1]);
						if(splits.length == 3) {
							ingredientId = Integer.parseInt(splits[2]);
						}
					}catch(Exception e) {
						res.sendError(HttpServletResponse.SC_BAD_REQUEST);
						return;
					}
					if(pizzaDAO.find(pizzaId) != null) {
						nouvPizza = pizzaDAO.find(pizzaId);
						if(ingredientDAO.find(ingredientId) != null) {
							Ingredient ingredToAdd = ingredientDAO.find(ingredientId);
							pizzaDAO.find(pizzaId).getGarniture().add(ingredToAdd);
							pizzaDAO.addIngredient(nouvPizza, ingredToAdd);
						}else {
							res.sendError(HttpServletResponse.SC_NOT_FOUND);
						}
					}else {
						res.sendError(HttpServletResponse.SC_NOT_FOUND);				
					}
				}else {
					try {
						nouvPizza = mapper.readValue(data.toString(), Pizzas.class);			
					}catch(Exception e) {
						res.sendError(HttpServletResponse.SC_BAD_REQUEST);
						return;
					}
					if(pizzaDAO.find(nouvPizza.getPzo())!= null) {
                        res.sendError(HttpServletResponse.SC_CONFLICT);
                        return;
                    }
					pizzaDAO.save(nouvPizza);
				}
			} else {
				res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			}
		}
		jsonToString = mapper.writeValueAsString(nouvPizza);
		out.println(jsonToString);
		out.println(mapper.writeValueAsString(data.toString()));
		out.close();
	}
	
	@Override
	public void doDelete( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException{
		res.setContentType("application/json;charset-UTF-8");
		PrintWriter out = res.getWriter();
		String info = req.getPathInfo();
		String[] splits;
		String authorization = req.getHeader("Authorization");
		String token = "";
		boolean isAuthorized;
		if (authorization == null || (!authorization.startsWith("Basic") && !authorization.startsWith("Bearer"))) {
			res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}else {
			if(authorization.startsWith("Bearer")) {
				token = authorization.substring("Bearer".length()).trim();
				isAuthorized = pizzaDAO.verifTokenJWT(token);
			}else {
				token = authorization.substring("Basic".length()).trim();
				isAuthorized = pizzaDAO.verifToken(token);
			}
			if(isAuthorized) {
				try {
					splits = info.split("/");
				} catch (Exception e) {
					splits = new String[1];
				}
				if (splits.length >= 2) {
					int idPizza;
					int idIngredient = -1;
					try {
						idPizza = Integer.parseInt(splits[1]);
						if (splits.length == 3) {
							idIngredient = Integer.parseInt(splits[2]);
						}
					} catch (Exception e) {
						res.sendError(HttpServletResponse.SC_BAD_REQUEST);
						return;
					}
					if (pizzaDAO.find(idPizza) != null) {
						if (splits.length == 2) {
							pizzaDAO.delete(idPizza);
							out.println("DELETION OF A PIZZA COMPLETED");
						} else if (pizzaDAO.findIngredient(idPizza, idIngredient) != null) {
							Ingredient ingToRemove = pizzaDAO.findIngredient(idPizza, idIngredient);
							pizzaDAO.find(idPizza).getGarniture().remove(ingToRemove);
							pizzaDAO.removeIngredient(idPizza, idIngredient);
							out.println("L'INGREDIENT A SUPPRIMÉ EST " + idIngredient + " SUR LA PIZZA " + idPizza);
						} else {
							res.sendError(HttpServletResponse.SC_NOT_FOUND);
							return;
						}
					} else {
						res.sendError(HttpServletResponse.SC_NOT_FOUND);
						return;
					}
				}
			} else {
				res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			}
		}
		out.close();
	}

	@Override
	public void doPatch(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
	    res.setContentType("application/json;charset-UTF-8");
	    PrintWriter out = res.getWriter();
	    String jsonToString;
	    ObjectMapper mapper = new ObjectMapper();
	    StringBuilder data = new StringBuilder();
	    BufferedReader reader = req.getReader();
	    String info = req.getPathInfo();
	    String[] splits;
	    String line;
	    String authorization = req.getHeader("Authorization");
		String token = "";
		boolean isAuthorized;
		if (authorization == null || (!authorization.startsWith("Basic") && !authorization.startsWith("Bearer"))) {
			res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		}else {
			if(authorization.startsWith("Bearer")) {
				token = authorization.substring("Bearer".length()).trim();
				isAuthorized = pizzaDAO.verifTokenJWT(token);
			}else {
				token = authorization.substring("Basic".length()).trim();
				isAuthorized = pizzaDAO.verifToken(token);
			}
			if(isAuthorized) {
				while ((line = reader.readLine()) != null) {
			        data.append(line);
			    }
			    try {
			        splits = info.split("/");
			    } catch (Exception e) {
			        splits = new String[1];
			    }
		
			    if (splits.length >= 2) {
			        int PizzaId;
			        try {
			            PizzaId = Integer.parseInt(splits[1]);
			        } catch (Exception e) {
			            res.sendError(HttpServletResponse.SC_BAD_REQUEST);
			            return;
			        }
			        Pizzas nouvPizza = pizzaDAO.find(PizzaId);
			        if (nouvPizza != null) {
			            JsonNode jsonNode = mapper.readTree(data.toString());
			            if (jsonNode.has("price")) {
			                double price = jsonNode.get("price").asDouble();
			                nouvPizza.setPrice(price);
			            }else if (jsonNode.has("name")) {
			                String name = jsonNode.get("name").asText();
			                nouvPizza.setName(name);
			            }else if (jsonNode.has("pate")) {
			                String pate = jsonNode.get("pate").asText();
			                nouvPizza.setPate(pate);
			            }else{
							res.sendError(HttpServletResponse.SC_BAD_REQUEST);
						}
			            pizzaDAO.update(nouvPizza);
			            jsonToString = mapper.writeValueAsString(nouvPizza);
			            out.println(jsonToString);
			        } else {
			            res.sendError(HttpServletResponse.SC_NOT_FOUND);
			        }
			    } else {
			        res.sendError(HttpServletResponse.SC_BAD_REQUEST);
			    }
			} else {
				res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			}
		}
	    out.close();
	}
}
